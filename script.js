// initmap s'exécute lorsque le DOM est chargé
window.onload = function(){
    initMap(); 
};


// Fonction Carte
function initMap(){

    // ----------------- Json local -----------------
    // 1. charger l'URL du fichier JSON 
    let requestURL = 'recup.json';
    // 2. instancier un nouvel objet avec new / Ajax
    let request = new XMLHttpRequest();
    // 3. ouvrir une nouvelle requête
    request.open('GET', requestURL);
    // 4. signaler au serveur que nous attendons une réponse au format JSON + envoi de requete
    request.responseType = 'json';
    request.send();
    // 5. nous stockons la réponse à notre requête (disponible au travers de la propriété response) dans la variable recupInit
    request.onload = function() {
        let recupInit = request.response;
        showOnMap(recupInit);
        showLocation(recupInit);
    }

    // On initialise la latitude et longitude, ici les coordonnées du languedoc
    let lat = 43.5912356;
    let lon = 3.2583626;

    // On initialise le tableau de marqueurs (utile pour gérer le zoom au chargement de la page)
    let tabMarkers = [];

    // On initialise la variable de cluster + la couche de layer (plugin LayerSupport)
    let markers = L.markerClusterGroup.layerSupport();

    // Initialisation de la map avec une valeur de zoom
    let map = L.map('map').setView([lat, lon], 8);

    // On initialise le tableau d'initiatives pour stocker les types d'initiatives
    let tabInitiatives = [];


    // ----------------------------------------------------------------------------------------------
    // AVEC OPENSTREETMAP
    // On récupére les tuiles (tiles) sur openstreetmap
    // L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
    //     attribution: '&copy; OpenStreetMap France | &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    //     minZoom: 1,
    //     maxZoom: 20
    // }).addTo(map);

    // ----------------------------------------------------------------------------------------------
    // AVEC CARTO
    // On récupére les tuiles (tiles) sur carto JS
    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 20
    }).addTo(map);

    // initialisation du client carto
    const client = new carto.Client({
        apiKey: 'rAKmXCPuk9N8jxz7oXWzyw',
        username: 'civl',
        serverUrl: 'https://swan.geodb.host/user/civl'
    })

    // Affichage des noms de villes
    L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}.png', {
        minZoom: 1,
        maxZoom: 20
    }).addTo(map);

    // ----------------------------------------------------------------------------------------------




    // Fonction qui affiche tous les markers sur la map
    function showOnMap(jsonObj){
        let location = jsonObj['location'];
        for (let i = 0; i < location.length; i++) {
            let marker = L.marker([location[i].localisation_latitude, location[i].localisation_longitude]);
            marker.bindPopup(`<b>${location[i].titre}</b>`);
            markers.addLayer(marker);
            tabMarkers.push(marker);
        }
        // On regroupe les marqueurs dans un groupe Leaflet
        let group = new L.featureGroup(tabMarkers);
        // On adapte le zoom au groupe
        map.fitBounds(group.getBounds());
        map.addLayer(markers);
    }



    // Fonction qui affiche les infos en dessous de la map
    function showLocation(jsonObj){

        let selectListFilter = document.querySelector('.list-filter');
        let location = jsonObj['location'];
        let myUl1 = document.createElement('ul');
        
        for (let i = 0; i < location.length; i++) {
            let listLi1 = document.createElement('li');
            // récupération nom des infos
            let titre = location[i].titre;
            let lieux = location[i].lieux;
            let latitude = location[i].localisation_latitude;
            let longitude = location[i].localisation_longitude;

            listLi1.textContent = (i+1) + '. ' + titre + ' - ' + lieux + ' (lat : ' + latitude + ' / lon : ' + longitude + ')';


            // On boucle sur les tags
            let tags = location[i]['tags'];
            if (tags.length !== 0) {
                let myUl2 = document.createElement('ul');
                for (let j = 0; j < tags.length; j++) {
                    let listLi2 = document.createElement('li');
                    let initiative = tags[j]['slug']['fr'];
                    listLi2.textContent = initiative;
                    myUl2.appendChild(listLi2);

                    // On remplie le tableau d'initiatives
                    tabInitiatives.push(initiative);
                }
                listLi1.appendChild(myUl2);
            }
            myUl1.appendChild(listLi1);


            // On boucle sur les categories
            let categories = location[i]['categories'];
            if (categories.length !== 0) {
                let myOl = document.createElement('ol');
                for (let j = 0; j < categories.length; j++) {
                    let listLi3 = document.createElement('li');
                    let catSlug = categories[j]['slug'];
                    listLi3.textContent = catSlug;
                    myOl.appendChild(listLi3);

                    // On remplie le tableau d'initiatives
                    tabInitiatives.push(catSlug);
                }
                listLi1.appendChild(myOl);
            }
            myUl1.appendChild(listLi1);


        }
        selectListFilter.appendChild(myUl1);

        // On filtre le tableau pour avoir des tags unique (pas trop utile)
        const filteredArray = tabInitiatives.filter( (ele,pos) => tabInitiatives.indexOf(ele) == pos);


        // Fonction qui remplie le champ filtre
        let selectFilterChoose = document.querySelector('#filter-choose');
        for (let i = 0; i < filteredArray.length; i++) {
            let tags = filteredArray[i];
            let listOption = document.createElement('option');
            listOption.textContent = tags;
            selectFilterChoose.appendChild(listOption);
        }


    }


}














































